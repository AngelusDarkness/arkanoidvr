﻿using UnityEngine;
using System.Collections;

public class DeadZone : MonoBehaviour {

	public GameManager gameManager;
	
	void OnTriggerEnter(Collider col)
	{
		Debug.Log("DeadZone: " + col.tag);
		if(col.CompareTag("Ball"))
		{
			gameManager.Die();
		}
	}
}
